#!/usr/bin/env bash
# SPDX-FileCopyrightText: Copyright © 2024 BlueBuild developers <bluebuild@xyny.anonaddy.com>
# SPDX-FileCopyrightText: Copyright © 2024-2025 Andrey Brusnik <dev@shdwchn.io>

# SPDX-License-Identifier: Apache-2.0

set -Eeuo pipefail

CONTAINER_DIR="/usr/etc/containers"
MODULE_DIRECTORY="${MODULE_DIRECTORY:-"/tmp/modules"}"
IMAGE_NAME_FILE="${IMAGE_NAME//\//_}"

echo "Setting up container signing in policy.json and cosign.yaml for $IMAGE_NAME"
echo "Registry to write: $IMAGE_REGISTRY"

if ! [[ -d "$CONTAINER_DIR" ]]; then
    mkdir -p "$CONTAINER_DIR"
fi

if ! [[ -d "${CONTAINER_DIR}/registries.d" ]]; then
   mkdir -p "${CONTAINER_DIR}/registries.d"
fi

if ! [[ -d "/usr/etc/pki/containers" ]]; then
    mkdir -p "/usr/etc/pki/containers"
fi

if ! [[ -f "${CONTAINER_DIR}/policy.json" ]]; then
    cp "${MODULE_DIRECTORY}/signing/policy.json" "${CONTAINER_DIR}/policy.json"
fi

if ! [[ -f "/usr/etc/pki/containers/${IMAGE_NAME_FILE}.pub" ]]; then
    cp "/usr/share/shadowblue/cosign.pub" "/usr/etc/pki/containers/${IMAGE_NAME_FILE}.pub"
fi

POLICY_FILE="$CONTAINER_DIR/policy.json"

jq --arg image_registry "${IMAGE_REGISTRY}" \
   --arg image_name "${IMAGE_NAME}" \
   --arg image_name_file "${IMAGE_NAME_FILE}" \
   '.transports.docker |= 
    { ($image_registry + "/" + $image_name): [
        {
            "type": "sigstoreSigned",
            "keyPath": ("/etc/pki/containers/" + $image_name_file + ".pub"),
            "signedIdentity": {
                "type": "matchRepository"
            }
        }
    ] } + .' "$POLICY_FILE" > /tmp/tmp-policy.json
mv /tmp/tmp-policy.json "$POLICY_FILE"

mv "${MODULE_DIRECTORY}/signing/registry-config.yaml" "${CONTAINER_DIR}/registries.d/${IMAGE_NAME_FILE}.yaml"
sed -i "s|quay.io/IMAGENAME|${IMAGE_REGISTRY}/${IMAGE_NAME}|g" "${CONTAINER_DIR}/registries.d/${IMAGE_NAME_FILE}.yaml"
