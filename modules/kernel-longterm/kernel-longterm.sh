#!/usr/bin/env bash
# SPDX-FileCopyrightText: Copyright © 2024-2025 Andrey Brusnik <dev@shdwchn.io>

# SPDX-License-Identifier: Apache-2.0

set -Eeuo pipefail

series=$(echo "$1" | jq -r 'try .["series"]')

if [[ ${series} == "null" ]]; then
    series="6.6"
fi

if grep -q Rawhide /etc/fedora-release; then
    frelver=rawhide
else
    frelver=$(rpm -E %fedora)
fi

rpm-ostree cliwrap install-to-root /

wget -O "/etc/yum.repos.d/_copr:copr.fedorainfracloud.org:kwizart:kernel-longterm-${series}.repo" "https://copr.fedorainfracloud.org/coprs/kwizart/kernel-longterm-${series}/repo/fedora-${frelver}/kwizart-kernel-longterm-${series}-fedora-${frelver}.repo"

if rpm -q --quiet kernel-devel kernel-devel-matched; then
    rpm-ostree override remove kernel kernel-{core,modules,modules-core,modules-extra,devel,devel-matched} \
        --install kernel-longterm --install kernel-longterm-core \
        --install kernel-longterm-modules --install kernel-longterm-modules-core --install kernel-longterm-modules-extra \
        --install kernel-longterm-devel --install kernel-longterm-devel-matched
else
    rpm-ostree override remove kernel kernel-{core,modules,modules-core,modules-extra} \
        --install kernel-longterm --install kernel-longterm-core \
        --install kernel-longterm-modules --install kernel-longterm-modules-core --install kernel-longterm-modules-extra
fi

rpm -q kernel-longterm # Check that kernel-longterm was installed. Idk why but rpm-ostree override might fail without crashing whole build!
