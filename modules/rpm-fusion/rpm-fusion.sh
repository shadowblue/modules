#!/usr/bin/env bash
# SPDX-FileCopyrightText: Copyright © 2024-2025 Andrey Brusnik <dev@shdwchn.io>

# SPDX-License-Identifier: Apache-2.0

set -Eeuo pipefail

nonfree=$(echo "$1" | jq -r 'try .["nonfree"]')

rpm-ostree install "https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm"

if [[ ${nonfree} == true ]]; then
    rpm-ostree install "https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm"
fi
