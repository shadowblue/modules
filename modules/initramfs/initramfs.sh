#!/usr/bin/env bash
# SPDX-FileCopyrightText: Copyright © 2024-2025 Andrey Brusnik <dev@shdwchn.io>

# SPDX-License-Identifier: Apache-2.0

set -Eeuo pipefail

rpm-ostree cliwrap install-to-root /

kernel_name=$(rpm -qa kernel-longterm kernel --queryformat '%{name}')
kernel_version=$(rpm -q "${kernel_name}" --queryformat '%{version}-%{release}.%{arch}')

/usr/libexec/rpm-ostree/wrapped/dracut --no-hostonly --kver "$kernel_version" --reproducible -v --add ostree -f "/lib/modules/${kernel_version}/initramfs.img"
